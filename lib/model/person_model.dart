class Person {
  String? name;
  String? gender;
  int? id;

  Person({this.name, this.gender, this.id});
}