import 'package:flutter/material.dart';

class DropDownButtonFormFieldWidget extends StatelessWidget {
  String? value;
  ValueChanged onChanged;
  List<String>? listSex;

  DropDownButtonFormFieldWidget(
      {Key? key, this.value, required this.onChanged, this.listSex})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      decoration: const InputDecoration(border: OutlineInputBorder()),
      isExpanded: true,
      value: value,
      onChanged: onChanged,
      items: listSex!.map((sex) {
        return DropdownMenuItem(
          value: sex,
          child: Text(sex),
        );
      }).toList(),
    );
  }
}
