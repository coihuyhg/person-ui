import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextFormFieldWidget extends StatelessWidget {
  TextEditingController? controller;
  String? hintText;
  bool obscureText;
  Widget? suffixIcon;
  TextInputType? keyboardType;
  List<TextInputFormatter>? inputFormatters;

  TextFormFieldWidget(
      {Key? key,
      this.controller,
      this.hintText,
      this.obscureText = false,
      this.suffixIcon,
      this.keyboardType,
      this.inputFormatters})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      controller: controller,
      obscureText: obscureText,
      inputFormatters: inputFormatters,
      decoration: InputDecoration(
        border: const OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(5))),
        suffixIcon: suffixIcon,
        hintText: hintText,
      ),
    );
  }
}
