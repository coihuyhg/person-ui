import 'package:flutter/material.dart';

class ButtonElevatedButton extends StatelessWidget {
  Color? color, textColor;
  VoidCallback? onPressed;
  String? text;

  ButtonElevatedButton(
      {Key? key, this.color, this.textColor, this.onPressed, this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ButtonStyle(
        backgroundColor: MaterialStatePropertyAll<Color>(color!),
      ),
      onPressed: onPressed,
      child: Text(
        text!,
        style: TextStyle(fontSize: 16, color: textColor),
      ),
    );
  }
}
