import 'package:flutter/material.dart';

import '../../model/person_model.dart';
import '../../widgets/button_widgets/button_elevated_button.dart';
import '../widgets/info_person_widget.dart';

class ListPerson extends StatefulWidget {
  const ListPerson({Key? key}) : super(key: key);

  @override
  State<ListPerson> createState() => _ListPersonState();
}

class _ListPersonState extends State<ListPerson> {
  late TextEditingController nameController = TextEditingController();
  late TextEditingController genderController = TextEditingController();
  late TextEditingController idController = TextEditingController();

  List personList = [];
  int? selectedIndex;
  String? selectedLocation = "Male";

  @override
  void dispose() {
    nameController.dispose();
    genderController.dispose();
    idController.dispose();
    super.dispose();
  }

  //Thêm thông tin người dùng
  void addPerson() {
    setState(() {
      Person person = Person(
          name: nameController.text,
          gender: genderController.text,
          id: int.parse(idController.text));
      if (selectedIndex == null) {
        personList.add(person);
      } else {
        personList[selectedIndex!] = person;
      }
      nameController.text = "";
      genderController.text = "";
      idController.text = "";
      selectedIndex = null;
    });
    ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(content: Text('Đã thêm thông tin')));
  }

  void savePerson(int? index) {
    Person newPerson = Person(
        name: nameController.text,
        gender: genderController.text,
        id: int.parse(idController.text));
    if (index != null) {
      editPerson(index, newPerson);
    } else {
      addPerson();
    }
    nameController.text = "";
    genderController.text = "";
    idController.text = "";
    Navigator.of(context).pop();
  }

  //Sửa thông tin người dùng
  void editPerson(int index, Person person) {
    setState(() {
      personList[index] = person;
    });
    ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(content: Text('Đã sửa thông tin')));
  }

  //Xóa thông tin người dùng
  void deletePerson(int index) {
    setState(() {
      personList.removeAt(index);
    });
    Navigator.of(context).pop();
    ScaffoldMessenger.of(context)
        .showSnackBar(const SnackBar(content: Text('Đã xóa thông tin')));
  }

  void showDialogPerson(int index) {
    Navigator.pop(context);
    showDialog(
      context: context,
      builder: (context) => Dialog(
        child: Padding(
          padding: const EdgeInsets.all(21),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Edit".toUpperCase(),
                style: const TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const SizedBox(height: 10),
              InfoPersonWidget(
                nameController: nameController,
                genderController: genderController,
                idController: idController,
                value: selectedLocation,
                onChanged: (newValue) {
                  genderController.text = newValue ?? '';
                  setState(() {
                    selectedLocation = newValue;
                  });
                },
              ),
              const SizedBox(height: 12),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ButtonElevatedButton(
                    color: const Color(0xffD9D9D9),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    text: "Cancel",
                    textColor: const Color(0xFF6B6B6B),
                  ),
                  const SizedBox(width: 16),
                  ButtonElevatedButton(
                    color: const Color(0xff00A328),
                    onPressed: () => savePerson(index),
                    text: "Save",
                    textColor: Colors.white,
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void showButtonSheetPerson(int index) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    showModalBottomSheet(
      context: context,
      builder: (context) {
        return Container(
          padding: const EdgeInsets.all(10),
          height: height * 0.33,
          child: Column(
            children: [
              Container(
                padding: const EdgeInsets.only(top: 12, bottom: 12),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                    border: Border.all()),
                child: Column(
                  children: [
                    const Text("Choose action",
                        style:
                            TextStyle(fontSize: 13, color: Color(0xffbeb7b7))),
                    const Divider(),
                    InkWell(
                      onTap: () {
                        nameController.text = personList[index].name;
                        genderController.text = personList[index].gender;
                        idController.text = personList[index].id.toString();
                        showDialogPerson(index);
                      },
                      child: const Text("Edit",
                          style: TextStyle(
                              fontSize: 20, color: Color(0xff0078FF))),
                    ),
                    const Divider(),
                    TextButton(
                        onPressed: () {
                          deletePerson(index);
                        },
                        child: const Text("Delete",
                            style: TextStyle(
                                fontSize: 24, color: Color(0xffFF1F00)))),
                  ],
                ),
              ),
              const SizedBox(height: 8),
              Container(
                padding: const EdgeInsets.all(16),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(14),
                    border: Border.all()),
                alignment: Alignment.center,
                width: width,
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: const Text("Cancel",
                      style: TextStyle(fontSize: 20, color: Color(0xff0078FF))),
                ),
              ),
            ],
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.only(
                  left: 49, right: 49, top: 20, bottom: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      const SizedBox(),
                      ButtonElevatedButton(
                        color: const Color(0xffEDEDED),
                        onPressed: addPerson,
                        text: "Thêm",
                        textColor: Colors.black,
                      ),
                    ],
                  ),
                  InfoPersonWidget(
                    nameController: nameController,
                    genderController: genderController,
                    idController: idController,
                    value: selectedLocation,
                    onChanged: (newValue) {
                      genderController.text = newValue ?? '';
                      setState(() {
                        selectedLocation = newValue;
                      });
                    },
                  ),
                ],
              ),
            ),
            const Divider(thickness: 10),
            Container(
              padding: const EdgeInsets.only(
                  left: 49, right: 49, top: 20, bottom: 20),
              height: 300,
              child: ListView.builder(
                itemCount: personList.length,
                itemBuilder: (context, index) {
                  Person person = personList[index];
                  return TextButton(
                    onPressed: () => showButtonSheetPerson(index),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          "${person.name}",
                          style: const TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: Colors.black),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text("${person.gender}",
                            style: const TextStyle(
                                fontSize: 14, color: Colors.black)),
                        const SizedBox(
                          height: 5,
                        ),
                        Text("${person.id}".replaceRange(0, 0, "****"),
                            style: const TextStyle(
                                fontSize: 14, color: Colors.black)),
                        const Divider(),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
