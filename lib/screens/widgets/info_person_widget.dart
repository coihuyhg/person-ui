import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../widgets/drop_down_button_widgets/drop_down_button_form_field_widget.dart';
import '../../../widgets/text_form_field_widgets/text_form_field_widget.dart';

class InfoPersonWidget extends StatefulWidget {
  late TextEditingController nameController = TextEditingController();
  late TextEditingController genderController = TextEditingController();
  late TextEditingController idController = TextEditingController();
  ValueChanged onChanged;
  String? value;

  InfoPersonWidget(
      {Key? key,
      required this.nameController,
      required this.genderController,
      required this.idController,
      required this.onChanged,
      this.value})
      : super(key: key);

  @override
  State<InfoPersonWidget> createState() => _InfoPersonWidgetState();
}

class _InfoPersonWidgetState extends State<InfoPersonWidget> {
  bool isHintId = true;

  void hideId() {
    setState(() {
      isHintId = !isHintId;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        const Text("Full Name", style: TextStyle(fontSize: 16)),
        const SizedBox(height: 7),
        TextFormFieldWidget(
          controller: widget.nameController,
          hintText: "Enter your name",
        ),
        const SizedBox(height: 25),
        const Text("Sex", style: TextStyle(fontSize: 16)),
        const SizedBox(height: 7),
        DropDownButtonFormFieldWidget(
          value: widget.value,
          onChanged: widget.onChanged,
          listSex: const ["Male", "Female"],
        ),
        const SizedBox(height: 25),
        const Text("Citizen ID", style: TextStyle(fontSize: 16)),
        const SizedBox(height: 7),
        TextFormFieldWidget(
          controller: widget.idController,
          obscureText: isHintId,
          inputFormatters: [FilteringTextInputFormatter.digitsOnly],
          keyboardType: TextInputType.number,
          suffixIcon: IconButton(
            onPressed: hideId,
            icon: isHintId
                ? const Icon(Icons.visibility)
                : const Icon(Icons.visibility_off),
          ),
        ),
      ],
    );
  }
}
